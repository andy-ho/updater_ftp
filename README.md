# updater_ftp

## 介绍
自动升级工具，支持ftp。



## 开发环境

- 工具：

  - Visual Studio 2015

- 依赖：

  - boost 1.5.9

  - [cpp_utils](https://gitee.com/andy-ho/cpp_utils.git)


## 工具介绍（updater_ftp_bin.rar）

使用Cpp写的自动升级工具，来源[https://gitee.com/andy-ho/updater_ftp](https://gitee.com/andy-ho/updater_ftp)

这里将项目编译后的二进制文件进行存档，方便后期项目使用。

## 依赖说明

- 编译工具：VS2015 Update3
- 运行依赖：Microsoft Visual C++ 2015 Redistributable Package(x86)

如果程序打不开，请手动安装运行依赖。

## 文件说明

```
Update.exe      //自动升级主程序，用于版本检查、文件下载、更新替换等功能
UpdateTool.exe  //客户端升级包打包&生成版本配置文件“filelist.xml”
release.bat     //客户端程序发布脚本
```

## 使用方法

### Update.exe

将改文件放到客户端目录中，在客户端启动时调用CheckUpdate()方法来检测并升级版本，具体参数见下方注释。

```
private void CheckUpdate()
{
    //使用方法： [update.exe filelist_url setup_path main_exe_path mod]
    //    模式mod        提示是否现在更新        是否结束主进程        是否提示更新结果
    //    d            √                        √                    √
    //    sd            √                        √                    ×
    //    sa            ×(替换)                ×                    ×
    //举例说明： [update.exe ftp://user:password@127.0.0.1:21/filelist.xml C:\\setup_test C:\\setup_test\\SecondHandCar.exe d]
    //           [update.exe http://127.0.0.1/filelist.xml C:\\setup_test C:\\setup_test\\SecondHandCar.exe d]
    ProcessHelper.Start(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "Update.exe", _setting.UpdateServerUrl + "/filelist.xml" + " " + System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + " " + Assembly.GetExecutingAssembly().Location + " sd" );
}
```

### UpdateTool.exe

