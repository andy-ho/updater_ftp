// Update.cpp : main source file for Update.exe
//

#include "stdafx.h"

#include "resource.h"

#include "MainDlg.h"

#include "updater.hpp"

CAppModule _Module;
INITIALIZE_EASYLOGGINGPP
//使用方法： [update.exe filelist_url setup_path main_exe_path mod]
//	模式mod		提示是否现在更新		是否结束主进程		是否提示更新结果
//	d			√					√					√
//	sd			√					√					×
//	sa			×(替换)				×					×
//举例说明： [update.exe ftp://user:password@127.0.0.1:21/filelist.xml C:\\setup_test C:\\setup_test\\SecondHandCar.exe d]
//          [update.exe http://127.0.0.1/filelist.xml C:\\setup_test C:\\setup_test\\SecondHandCar.exe d]
int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR /*lpstrCmdLine*/, int /*nCmdShow*/)
{
	HRESULT hRes = ::CoInitialize(NULL);
// If you are running on NT 4.0 or higher you can use the following call instead to 
// make the EXE free threaded. This means that calls come in on a random RPC thread.
//	HRESULT hRes = ::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	ATLASSERT(SUCCEEDED(hRes));

	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hRes = _Module.Init(NULL, hInstance);
	ATLASSERT(SUCCEEDED(hRes));

	based::log::init_log();

	int nArgs = 0;
	LPWSTR *szArglist = NULL;
	szArglist = CommandLineToArgvW(::GetCommandLine(), &nArgs);
	if (szArglist == NULL || nArgs != 5)
	{
		//LOG(ERROR) << "参数输入错误，使用方法：[update.exe filelist_url setup_path main_exe_path mod]";
		LocalFree(szArglist);
		exit(0);
	}

	int nRet = 0;
	// BLOCK: Run application
	{
		CMainDlg dlgMain(szArglist[1], szArglist[2], szArglist[3], szArglist[4]);
		nRet = dlgMain.DoModal();
	}

	_Module.Term();
	::CoUninitialize();

	LocalFree(szArglist);

	return nRet;
}
