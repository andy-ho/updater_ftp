// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "MainDlg.h"

CMainDlg::CMainDlg(CString filelist_url, CString setup_path, CString main_exe_path, CString mod)
{
	m_filelist_url = filelist_url;
	m_setup_path = setup_path;
	m_main_exe_path = main_exe_path;
	m_update_mod = mod;
}

LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// center the dialog on the screen
	CenterWindow();

	// set icons
	HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON), LR_DEFAULTCOLOR);
	SetIcon(hIcon, TRUE);
	HICON hIconSmall = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
	SetIcon(hIconSmall, FALSE);

   // 绑定控件.
   m_main_progress.Attach(GetDlgItem(IDC_PGS_ALL));
   m_single_progress.Attach(GetDlgItem(IDC_PGS_SINGLE));
   m_main_text.Attach(GetDlgItem(IDC_TEXT_MAIN));
   m_single_text.Attach(GetDlgItem(IDC_TEXT_SINGLE));

   // 设置控件.
   m_single_progress.SetRange(0, 1000);
   m_main_progress.SetRange(0, 1000);

   m_status = 0;

   //启动检测升级程序
   SetTimer(START_UPDATE_TIMER, 1000);

	return TRUE;
}

LRESULT CMainDlg::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CSimpleDialog<IDD_ABOUTBOX, FALSE> dlg;
	dlg.DoModal();
	return 0;
}

LRESULT CMainDlg::OnOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if (m_updater.result() == updater::st_updating)
		return 0;
	m_updater.start(based::charset_convert().wchars_to_chars(m_filelist_url),
      boost::bind(&CMainDlg::check_files_callback, this, _1, _2, _3),
      boost::bind(&CMainDlg::down_load_callback, this, _1, _2, _3, _4, _5, _6, _7),
      boost::bind(&CMainDlg::check_files_callback, this, _1, _2, _3),
      boost::bind(&CMainDlg::update_files_process, this, _1, _2, _3,_4),
      based::charset_convert().wchars_to_chars(m_setup_path));
	std::thread([&]() {
		while (m_updater.result() == updater::st_updating){
			Sleep(100);
		}

		PostMessage(WM_UPDATE_RESULT, (WPARAM)m_updater.result());
	}).detach();
	return 0;
}

LRESULT CMainDlg::OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	m_updater.stop();
	EndDialog(wID);
	return 0;
}

void CMainDlg::check_files_callback(std::string file, int count, int index)
{
   CString str = file.c_str();
   msg_info* info = new msg_info;

   m_status = WM_CHECK_PROGRESS;

   info->text.Format(_T("总计:%d, 当前:%d, 正在检测文件: \'%s\'"), count, index + 1, str.GetBuffer(0));
   if (count == index + 1)
      info->text = _T("检查完成.");

   info->main_pos = (double)(index + 1) / (double)count * 1000.0f;
   info->single_pos = 1000.0f;

   PostMessage(WM_CHECK_PROGRESS, (WPARAM)info);
}

void CMainDlg::down_load_callback(std::string file, int count, int index, 
   int total_size, int total_read_bytes, int file_size, int read_bytes)
{
   CString str = file.c_str();
   msg_info* info = new msg_info;

   m_status = WM_WORK_PROGRESS;

   info->text.Format(_T("当前: %d, 共: %d, 正在下载文件: \'%s\'"), index + 1, count, str.GetBuffer(0));

   if (count == index + 1)
      info->text = _T("下载完成.");

   info->main_pos = (double)total_read_bytes / (double)total_size * 1000.0f;
   info->single_pos = (double)read_bytes / (double)file_size * 1000.0f;

   PostMessage(WM_WORK_PROGRESS, (WPARAM)info);
}

bool CMainDlg::update_files_process(std::string file, int count, int index, int mandatory_update)
{
	if (file == "")
	{
		if (m_update_mod == "sa")
			return true;
		if (mandatory_update == 0)
		{
			if (MessageBox(_T("发现新版本，是否现在升级？"), _T("提示"), MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				std::string main_exe_name = boost::filesystem::path(m_main_exe_path).leaf().string();
				based::os::kill_process(based::charset_convert().chars_to_wchars(main_exe_name.c_str()), true, false);
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (mandatory_update == 1)
		{
			MessageBox(_T("发现新版本，此版本有重要升级，请保存好当前数据，点击确定按钮升级！"), _T("提示"), MB_OK | MB_ICONINFORMATION);
			std::string main_exe_name = boost::filesystem::path(m_main_exe_path).leaf().string();
			based::os::kill_process(based::charset_convert().chars_to_wchars(main_exe_name.c_str()), true, false);
			return true;
		}		
	}
	else
	{
		CString str = file.c_str();
		msg_info* info = new msg_info;

		m_status = WM_CHECK_PROGRESS;

		info->text.Format(_T("总计:%d, 当前:%d, 正在替换文件: \'%s\'"), count, index + 1, str.GetBuffer(0));
		if (count == index + 1)
			info->text = _T("替换完成.");

		info->main_pos = (double)(index + 1) / (double)count * 1000.0f;
		info->single_pos = 1000.0f;

		PostMessage(WM_CHECK_PROGRESS, (WPARAM)info);
	}
}

LRESULT CMainDlg::OnCheckProgress(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
   msg_info* info = (msg_info*)wParam;
   if (!info)
      return 0;

   if (m_status != WM_CHECK_PROGRESS)
   {
      m_status = WM_CHECK_PROGRESS;
      m_single_progress.SetPos(0);
      m_main_progress.SetPos(0);
   }

   if (info->text != m_single_str)
   {
      m_single_str = info->text;
      m_single_text.SetWindowText(m_single_str);
   }

   m_single_progress.SetPos(info->single_pos);
   m_main_progress.SetPos(info->main_pos);

   delete info;

   return 0;
}

LRESULT CMainDlg::OnWorkProgress(UINT /*uMsg*/, WPARAM wParam, LPARAM lParam, BOOL& /*bHandled*/)
{
   msg_info* info = (msg_info*)wParam;
   if (!info)
      return 0;

   if (m_status != WM_WORK_PROGRESS)
   {
      m_status = WM_WORK_PROGRESS;
      m_single_progress.SetPos(0);
      m_main_progress.SetPos(0);
   }

   if (info->text != m_single_str)
   {
      m_single_str = info->text;
      m_single_text.SetWindowText(m_single_str);
   }

   m_single_progress.SetPos(info->single_pos);
   m_main_progress.SetPos(info->main_pos);

   delete info;

   return 0;
}

LRESULT CMainDlg::OnUpdateProgress(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
   return 0;
}

LRESULT CMainDlg::OnUpdateResult(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (m_update_mod == "sd")
	{
		if (wParam == updater::st_succeed)
		{
			WinExec(based::charset_convert().wchars_to_chars(m_main_exe_path), SW_SHOW);
		}
	}
	else if (m_update_mod == "sa")
	{
	}
	else if (wParam == updater::st_error)
	{
		MessageBox(_T("很抱歉，更新出错！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else if (wParam == updater::st_succeed)
	{
		MessageBox(_T("恭喜你，更新成功！"), _T("温馨提示"), MB_ICONASTERISK);
		WinExec(based::charset_convert().wchars_to_chars(m_main_exe_path), SW_SHOW);
	}	
	else if (wParam == updater::st_no_need_update)
	{
		MessageBox(_T("最新版本，无需更新！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else if (wParam == updater::st_unable_to_connect)
	{
		MessageBox(_T("很抱歉，无法连接到更新服务器！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else if (wParam == updater::st_invalid_http_response)
	{
		MessageBox(_T("很抱歉，http错误的返回！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else if (wParam == updater::st_open_file_failed)
	{
		MessageBox(_T("很抱歉，打开文件失败！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else if (wParam == updater::st_next_time_update)
	{
		MessageBox(_T("我们将会在下次给您升级！"), _T("温馨提示"), MB_ICONASTERISK);
	}
	else
	{
		MessageBox(_T("很抱歉，未知结果！"), _T("温馨提示"), MB_ICONASTERISK);
	}

	m_updater.stop();
	EndDialog(0);
	return 0;
}


LRESULT CMainDlg::OnTimer(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// TODO: Add your message handler code here and/or call default
	if (wParam == START_UPDATE_TIMER)
	{
		KillTimer(START_UPDATE_TIMER);
		BOOL a=0;
		OnOK(0, 0, NULL, a);
	}
	return 0;
}
